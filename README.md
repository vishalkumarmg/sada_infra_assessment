# Sada_infra_assessment

Challenge 1: check IP of devices connected in a network


# Summary


Challenge 1: check IP of devices connected in a network


Step - 1: List information on all network devices on your machine. IP addresses, link-level addresses, network masks, etc. per network device on your machine.

Step - 2: Pick a LAN device (e.g. ethernet) from the list. Do not pick a pseudo device, but pick a real device which is
attached to the real live network. This may be a real physical ethernet device or wifi device. Be sure to pick the currently active and attached network interface. For example, the main ethernet or wifi interface through which you are connected to the LAN that is attached to the router's main internal network in your home or office.

Step - 3: Find the IP range that are possible on that LAN.  For example, if your device chosen in step #2 was called "eth1" and it had IP address 192.168.1.100 and network mask of 255.255.255.0, then you will probably
use a range of 192.168.1.1 through 192.168.1.254.   The IP range may be different in your case, depending on which interface you chose in step 2. It may also be different due to different network mask on the chosen interface.

Step - 4: Write a simple script that will iterate through IP address range (for example, 192.168.1.1 through 192.168.1.254)
and ping each once and see if response was received successfully and finally print out a list of IP addresses of the machines that responded on your local network.

# How to use it

TO RUN THE PYTHON FILE: [MAC and linux users] If python is installed in your system, please run it using "python3 script.py".

No need to install any packages in particular.
Imported os and ipaddress library.

# Documentation

The entire script is written in python.

All the necessary functionalities are being taken care in the python script. (script.py)

Step - 1 : To list all the information used the command ('arp -a').

To run the commands using python, utilized the os package available in python.

Step - 2 : 

-> To pick the real LAN network device, utilized a command ('ipconfig getifaddr en0') which gives the ip address of the first ethernet device.

-> To get the subnet mask(network mask), utilized a command ('ipconfig getoption en0 subnet_mask') which will give the subnet mask of the network device.

Step - 3 :

-> Take the IP address and subnet mask from step-2. First, validate if it's a valid ip and subnet mask. 
-> Once we have a valid ip address and subnet mask. We convert them into binary and make each binary octet of 8 bit length by padding zeros.
-> Join the binary octets to form a binary mask.
-> Calculated the number of hosts, network and broadcast address to get the first and last ip range.

Step - 4 : 

-> Once we have the first and last ip range, we just iterate over the ip address and execute a command ('ping -c 1 IP_ADDRESS') which will ping the ip address once and see if we receive a response or not. If we receive the response, we store the ipaddress in the resultant array.
